# generic-parser

A utility used by [Kores](https://gitlab.com/Kores/Kores) to parse Java generic signatures and generic type arguments.