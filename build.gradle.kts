import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.30"
    id("com.github.hierynomus.license") version "0.16.1"
    application
    id("antlr")
    id("maven-publish")
}

group = "com.koresframework"
version = "0.2.1"

repositories {
    mavenCentral()
}

dependencies {
    antlr("org.antlr:antlr4:4.9.2")
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.3.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")

    testImplementation("io.kotest:kotest-runner-junit5:4.6.1")
    testImplementation("io.kotest:kotest-assertions-core:4.6.1")
    testImplementation("io.kotest:kotest-property:4.6.1")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.named("compileJava") {
    dependsOn("generateGrammarSource")
}

tasks.named("compileKotlin") {
    dependsOn("generateGrammarSource")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "16"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=all")
}

tasks.withType<nl.javadude.gradle.plugins.license.License> {
    header = rootProject.file("LICENSE_HEADER")
    strictCheck = true
    mapping("g4", "JAVADOC_STYLE")
    exclude { it.path.startsWith("$buildDir/generated-src") || it.path.endsWith(".g4") }
}

tasks.generateGrammarSource {
    arguments = arguments + listOf("-package", "com.koresframework.genericparser.parser", "-listener", "-visitor")
    finalizedBy("licenseFormat")
}

tasks.create<Exec>("grun") {
    dependsOn("generateGrammarSource")
    dependsOn("build")
    this.workingDir(file("$buildDir/classes/java/main"))
    this.commandLine("grun", "com.koresframework.genericparser.parser.Generic", "unit", "-gui", "$projectDir/test")
}

val sourcesJar = tasks.create<Jar>("sourcesJar") {
    dependsOn("classes")
    this.archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    repositories {
        maven {
            name = "Local"
            // change to point to your repo, e.g. http://my.org/repo
            url = uri("$buildDir/repo")
        }
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/29701822/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
        maven {
            name = "GitLabJgang"
            url = uri("https://gitlab.com/api/v4/projects/30392813/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        register("maven", MavenPublication::class) {
            from(components["kotlin"])
            artifactId = "generic-parser"
            artifact(sourcesJar)
        }
    }
}