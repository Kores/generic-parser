/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser

/**
 * ## Description
 *
 * Responsible for creating containers to hold types and type bounds.
 *
 * **generic-parser** supports both mutable and immutable containers, since value returned by [AddBoundFunction]
 * replaces the old stored value.
 *
 * ## Example
 *
 * An example of generic container factory and functions can be found at [com.koresframework.genericparser.container].
 *
 */
fun interface ContainerFactory<C> {
    fun createGenericContainer(wildcardTypeOrName: WildcardTypeOrName): C
}

/**
 * ## Description
 *
 * Responsible for adding bounds to an existing [generic container][C]. The result value replaces the old ones,
 * this means that both mutable and immutable containers are supported.
 *
 * ## Example
 *
 * An example of generic container factory and functions can be found at [com.koresframework.genericparser.container].
 *
 */
fun interface AddBoundFunction<C> {
    fun addBound(
        genericContainer: C,
        boundKind: BoundKind,
        type: C
    ): C
}

/**
 * Resolves whether a [String] is [VariableKind.Name], [VariableKind.Type] or [VariableKind.Wildcard].
 *
 * This resolver can be used to resolve ambiguous names that are not being resolved to the correct type, and can be proved
 * at runtime to be one or another.
 *
 * But be careful, the implementation must know all [strings][String] that are *TypeVariables* or *Classes**, to avoid
 * unexpected results.
 */
fun interface VariableKindResolver<C> {
    fun resolve(
        predicted: VariableKind,
        name: String
    ): VariableKind

    companion object {
        fun <C> defaultResolver() = VariableKindResolver<C> { predicated, _ -> predicated }
    }
}