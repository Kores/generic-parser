/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.parser

import com.koresframework.genericparser.*
import com.koresframework.genericparser.coroutine.CoGenericBaseVisitor
import com.koresframework.genericparser.coroutine.stacklessIf
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext

/**
 * ## Description
 *
 * Parses *Java Generic Signatures* and *Generic Type Arguments* into user-defined containers. This implementation uses
 * [ANTLR4](https://www.antlr.org/) generated lexer and parser.
 *
 * ### Visitor
 *
 * This implementation uses [ANTLR4 Visitor Pattern](https://github.com/antlr/antlr4/blob/master/doc/tool-options.md#-visitor)
 * in order to translate ANTLR4 Tree into user-defined container types.
 *
 * However, [VisitorGenericRepresentationParser] does have a [stacklessMode] which alleviates [stack overflow errors][StackOverflowError]
 * that may happen with visitor-pattern (as visitor pattern uses the stack to track the visit).
 *
 * ### Stackless
 * The [stackless mode][stacklessMode] uses Kotlin Coroutines to do “stackless” calls, which instead of using **stack allocations**,
 * uses the **heap** to store states and what happens after a call.
 *
 * The default mode of this feature is disabled, because it only pays the impact for deep generic representations. Tests
 * showed that *stackful-mode* is able to handle generic representations with a deepness up to **400**
 * (`List<? extends List<? extends ...>>` 400 times), while **stackless-mode* can go further.
 *
 * Performance-wise, both performs almost the same, but **stackless** has the disadvantage of being harder to debug.
 *
 * ## Example
 *
 * Example of generic representations that can be parsed:
 *
 * - `<B extends K & V, K, V>`
 * - `List<? extends CharSequence>`
 * - `List<? extends List<?>>`
 * - `<K, V extends List<? extends List<K>>>`
 *
 * ### Example of containers
 *
 * You can find example of containers [here][com.koresframework.genericparser.container].
 *
 * ### Usage example
 *
 * ```kotlin
 * val parser = createVisitorParser(GenericTypeFactory, GenericTypeAddBoundFunction)
 * val parsed = parser.parse("<T extends List<? extends CharSequence>>", parseGenericSignature())
 * ...
 * ```
 *
 * @see com.koresframework.genericparser.container
 */
class VisitorGenericRepresentationParser<C>(
    val containerFactory: ContainerFactory<C>,
    val genericAddBoundFunction: AddBoundFunction<C>,
    val variableKindResolver: VariableKindResolver<C> = VariableKindResolver.defaultResolver(),
    val stacklessMode: Boolean = false
) : GenericRepresentationParser<C> {
    override suspend fun parse(text: String, options: ParseOptions): List<C> {
        val lexer = GenericLexer(CharStreams.fromString(text))
        val tokens = CommonTokenStream(lexer)
        val parser = GenericParser(tokens)

        val listener = Listener(
            stacklessMode,
            containerFactory,
            genericAddBoundFunction,
            variableKindResolver,
            options
        )

        val containers = (listener.visit(parser.rootArgument()) as GenericData.ListOf<C>).data.map {
            listener.toContainer(it)
        }

        return containers
    }
}

fun <C> createVisitorParser(
    containerFactory: ContainerFactory<C>,
    genericAddBoundFunction: AddBoundFunction<C>,
    variableKindResolver: VariableKindResolver<C> = VariableKindResolver.defaultResolver(),
    stacklessMode: Boolean = false
) = VisitorGenericRepresentationParser(
    containerFactory = containerFactory,
    genericAddBoundFunction = genericAddBoundFunction,
    variableKindResolver = variableKindResolver,
    stacklessMode = stacklessMode,
)

fun parseGenericSignature(): ParseOptions = ParseOptions(
    parseMode = ParseMode.GenericSignature
)

fun parseTypeArguments(): ParseOptions = ParseOptions(
    parseMode = ParseMode.TypeArgument
)

sealed class GenericData<C> {
    open fun contains(other: GenericData<C>): Boolean = false

    data class ListOf<C>(val data: List<GenericData<C>>) : GenericData<C>() {
        override fun contains(other: GenericData<C>): Boolean = this.data.contains(other)
    }

    data class Container<C>(val container: C) : GenericData<C>() {
        override fun contains(other: GenericData<C>): Boolean =
            when (other) {
                is ListOf<C> -> false
                is Container<C> -> other.container == this.container
            }
    }
}

internal class Listener<C>(
    val stackless: Boolean,
    val containerFactory: ContainerFactory<C>,
    val genericAddBoundFunction: AddBoundFunction<C>,
    val variableKindResolver: VariableKindResolver<C>,
    val options: ParseOptions
) : CoGenericBaseVisitor<GenericData<C>>() {
    private val knownTypeVariables = options.knownTypeVariables
    private val parseMode = options.parseMode
    private val privateKnownTypeVariables = knownTypeVariables.toMutableSet()
    private val isTypeVariableMap = mutableMapOf<ParserRuleContext, Boolean>()
    private val knownTypesContainer = mutableMapOf<ParserRuleContext, GenericData.Container<C>>()

    override suspend fun visitRootArgument(ctx: GenericParser.RootArgumentContext): GenericData.ListOf<C> {
        suspend fun visitRootArguments(ctx: List<GenericParser.TypeArgumentContext>) =
            ctx.map {
                if (it.referenceType() != null && parseMode == ParseMode.GenericSignature) {
                    isTypeVariableMap[it.referenceType()] = true
                    knownTypesContainer[it] = visitReferenceType(it.referenceType())
                }
            }

        val list = if (ctx.typeArguments() != null) {
            visitRootArguments(ctx.typeArguments().typeArgumentList().typeArgument())
            visitTypeArguments(ctx.typeArguments())
        } else {
            visitRootArguments(ctx.typeArgumentList().typeArgument())
            visitTypeArgumentList(ctx.typeArgumentList())
        }

        return list
    }

    override suspend fun visitTypeArguments(ctx: GenericParser.TypeArgumentsContext): GenericData.ListOf<C> =
        stacklessIf(::stackless) {
            visitTypeArgumentList(ctx.typeArgumentList())
        }

    override suspend fun visitTypeArgumentList(ctx: GenericParser.TypeArgumentListContext): GenericData.ListOf<C> =
        stacklessIf(::stackless) {
            val knownTypes = if (parseMode == ParseMode.GenericSignature) {
                ctx.typeArgument().map {
                    knownTypesContainer[it]
                }
            } else {
                List(ctx.typeArgument().size) {
                    null
                }
            }

            val args = ctx.typeArgument().mapIndexed { index, typeArgumentContext ->
                if (typeArgumentContext.referenceType() != null) {
                    val result = knownTypes[index] ?: visitReferenceType(typeArgumentContext.referenceType())
                    val bounds = typeArgumentContext.wildcardBounds()
                    if (bounds != null) {
                        val visited = visitReferenceTypeWithIntersection(bounds.referenceTypeWithIntersection())
                        if (bounds.EXTENDS() != null) {
                            GenericData.Container(toContainer(result, BoundKind.Extends, visited))
                        } else {
                            GenericData.Container(toContainer(result, BoundKind.Super, visited))
                        }
                    } else {
                        result
                    }
                } else if (typeArgumentContext.wildcard() != null) {
                    visitWildcard(typeArgumentContext.wildcard())
                } else {
                    visitWildcard(typeArgumentContext.wildcard())
                }
            }

            GenericData.ListOf(args)
        }

    private suspend fun toContainer(
        root: GenericData<C>,
        kind: BoundKind,
        bounds: GenericData<C>
    ): C = stacklessIf(::stackless) {
        if (root.contains(bounds)) return@stacklessIf toContainer(bounds)

        val rootContainer = toContainer(root)

        when (bounds) {
            is GenericData.Container -> genericAddBoundFunction.addBound(rootContainer, kind, bounds.container)
            is GenericData.ListOf -> if (bounds.data.isEmpty())
                rootContainer
            else
                bounds.data.map {
                    toContainer(bounds, kind, it)
                }.fold(rootContainer) { root, c ->
                    genericAddBoundFunction.addBound(
                        root,
                        kind,
                        c
                    )
                }
        }
    }

    internal suspend fun toContainer(root: GenericData<C>) = stacklessIf(::stackless) {
        when (root) {
            is GenericData.Container -> root.container
            else -> throw IllegalStateException("Failed to convert `$root` to GenericData.Container")
        }
    }

    override suspend fun visitReferenceType(ctx: GenericParser.ReferenceTypeContext): GenericData.Container<C> =
        stacklessIf(::stackless) {
            val isVariableName =
                this.isTypeVariableMap[ctx] == true || this.privateKnownTypeVariables.contains(ctx.text)
            val predict = if (isVariableName) VariableKind.Name else VariableKind.Type
            val kind = this.variableKindResolver.resolve(predict, ctx.text)

            if (kind == VariableKind.Name) {
                val typeVar = ctx.text
                this.privateKnownTypeVariables.add(typeVar)
                GenericData.Container(containerFactory.createGenericContainer(WildcardTypeOrName.Name(ctx.text)))
            } else {
                if (ctx.classOrInterfaceType() != null) {
                    visitClassOrInterfaceType(ctx.classOrInterfaceType())
                } else {
                    GenericData.Container(containerFactory.createGenericContainer(WildcardTypeOrName.Type(ctx.text)))
                }
            }
        }

    override suspend fun visitClassOrInterfaceType(ctx: GenericParser.ClassOrInterfaceTypeContext): GenericData.Container<C> =
        stacklessIf(::stackless) {
            val name = ctx.typeName()
            val withArgs = ctx.children.firstOrNull {
                when (it) {
                    is GenericParser.ClassType_lfno_classOrInterfaceTypeContext -> it.typeArguments() != null
                    is GenericParser.InterfaceType_lfno_classOrInterfaceTypeContext -> it.classType_lfno_classOrInterfaceType().typeArguments() != null
                    is GenericParser.ClassType_lf_classOrInterfaceTypeContext -> it.typeArguments() != null
                    is GenericParser.InterfaceType_lf_classOrInterfaceTypeContext -> it.classType_lf_classOrInterfaceType().typeArguments() != null
                    else -> throw IllegalStateException("Unknown token: $it")
                }
            }

            if (withArgs is ParserRuleContext) {
                visitType(name, withArgs, withArgs.args())
            } else {
                visitType(name, ctx, null)
            }
        }

    override suspend fun visitClassType_lfno_classOrInterfaceType(ctx: GenericParser.ClassType_lfno_classOrInterfaceTypeContext): GenericData.Container<C> =
        stacklessIf(::stackless) {
            visitType(ctx.typeName(), ctx, ctx.typeArguments())
        }

    private suspend fun visitType(
        typeName: String,
        ctx: ParserRuleContext,
        typeArgs: GenericParser.TypeArgumentsContext?
    ): GenericData.Container<C> =
        stacklessIf(::stackless) {
            val id = typeName

            val isVariableName = this.isTypeVariableMap[ctx] == true || this.privateKnownTypeVariables.contains(id)
            val predict =
                if (isVariableName) VariableKind.Name else if (id == "?") VariableKind.Wildcard else VariableKind.Type

            val wildcardTypeOrName = when (this.variableKindResolver.resolve(predict, id)) {
                VariableKind.Name -> WildcardTypeOrName.Name(id)
                VariableKind.Wildcard -> WildcardTypeOrName.Wildcard
                else -> WildcardTypeOrName.Type(id)
            }

            val c = containerFactory.createGenericContainer(wildcardTypeOrName)
            val container = GenericData.Container(c)

            if (typeArgs != null) {
                val of = visitTypeArguments(typeArgs)
                val containerWithGenerics = toContainer(container, BoundKind.Of, of)

                GenericData.Container(containerWithGenerics)
            } else {
                GenericData.Container(c)
            }
        }


    override suspend fun visitClassType_lf_classOrInterfaceType(ctx: GenericParser.ClassType_lf_classOrInterfaceTypeContext): GenericData<C>? {
        return super.visitClassType_lf_classOrInterfaceType(ctx)
    }

    private fun GenericParser.ClassOrInterfaceTypeContext.typeName() =
        this.children.joinToString(separator = ".") {
            when (it) {
                is GenericParser.ClassType_lfno_classOrInterfaceTypeContext -> it.identifier().text
                is GenericParser.InterfaceType_lfno_classOrInterfaceTypeContext -> it.classType_lfno_classOrInterfaceType()
                    .identifier().text
                is GenericParser.ClassType_lf_classOrInterfaceTypeContext -> it.identifier().text
                is GenericParser.InterfaceType_lf_classOrInterfaceTypeContext -> it.classType_lf_classOrInterfaceType()
                    .identifier().text
                else -> throw IllegalStateException("Unknown token: $it")
            }
        }

    private fun ParserRuleContext.args() =
        when (this) {
            is GenericParser.ClassType_lfno_classOrInterfaceTypeContext -> this.typeArguments()
            is GenericParser.InterfaceType_lfno_classOrInterfaceTypeContext -> this.classType_lfno_classOrInterfaceType().typeArguments()
            is GenericParser.ClassType_lf_classOrInterfaceTypeContext -> this.typeArguments()
            is GenericParser.InterfaceType_lf_classOrInterfaceTypeContext -> this.classType_lf_classOrInterfaceType().typeArguments()
            else -> throw IllegalStateException("Unknown token: $this")
        }

    private fun GenericParser.ClassType_lfno_classOrInterfaceTypeContext.typeName() =
        (this.parent as GenericParser.ClassOrInterfaceTypeContext).typeName()

    override suspend fun visitInterfaceType_lfno_classOrInterfaceType(ctx: GenericParser.InterfaceType_lfno_classOrInterfaceTypeContext): GenericData.Container<C> {
        return visitClassType_lfno_classOrInterfaceType(ctx.classType_lfno_classOrInterfaceType())
    }

    override suspend fun visitReferenceTypeWithIntersection(ctx: GenericParser.ReferenceTypeWithIntersectionContext): GenericData.ListOf<C> =
        stacklessIf(::stackless) {
            if (ctx.referenceType().size == 1) {
                GenericData.ListOf(listOf(visitReferenceType(ctx.referenceType(0))))
            } else {
                GenericData.ListOf(ctx.referenceType().map { visitReferenceType(it) })
            }
        }


    override suspend fun visitWildcard(ctx: GenericParser.WildcardContext): GenericData.Container<C> =
        stacklessIf(::stackless) {
            if (ctx.wildcardBounds() == null) {
                GenericData.Container(containerFactory.createGenericContainer(WildcardTypeOrName.Wildcard))
            } else {
                visitWildcardBounds(ctx.wildcardBounds())
            }
        }

    override suspend fun visitWildcardBounds(ctx: GenericParser.WildcardBoundsContext): GenericData.Container<C> =
        stacklessIf(::stackless) {
            val kind = if (ctx.EXTENDS() != null) {
                BoundKind.Extends
            } else if (ctx.SUPER() != null) {
                BoundKind.Super
            } else {
                BoundKind.Of
            }

            val bounds = visitReferenceTypeWithIntersection(ctx.referenceTypeWithIntersection())

            val container = containerFactory.createGenericContainer(WildcardTypeOrName.Wildcard)

            GenericData.Container(toContainer(GenericData.Container(container), kind, bounds))
        }

}
