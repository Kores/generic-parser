/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume


/**
 * Launches a Job which completes immediately, this is used to implement `ParseTreeVisitor`s without call stack.
 *
 * With this function you can have recursion without [StackOverflowError] ever happening. However, memory pressure is
 * higher using this approach than using plain recursive calls and this negatively impacts the performance.
 *
 * For example:
 *
 * ```kotlin
 * fun recurse(value: Int) = if (value == 0) value else recurse(value - 1)
 * ```
 *
 * Throws [StackOverflowError] with high values, like `100000`, while:
 *
 * ```kotlin
 * suspend fun recurse(value: Int) = if (value == 0) value else stackless { recurse(value - 1) }
 * ```
 *
 * Can go higher than the plain recursive approach.
 *
 * ## Downsides
 *
 * The implementation of “[stackless] calls” relies on coroutine-launching logic, this does mean that a new coroutine
 * is launched to complete the computation.
 *
 * What it does really means? It means that the provided [function][f] will be executed with a dedicated [Job]
 * and [scope][CoroutineScope].
 *
 * Programmatically talking:
 * ```kotlin
 * suspend fun <E> find(node: Node<E>, predicate: (E) -> Boolean): Node<E>? = stackless { // Suspends and launches a coroutine to complete
 *     if (predicate(node.value)) node
 *     else find(node.left, predicate) /* Suspends and launches a coroutine to complete */ ?: find(node.right, predicate) /* Suspends and launches a coroutine to complete */
 *     //...
 * }
 *
 * suspend fun example() = stackless { // Suspends and launches a coroutine to complete
 *     val node = Node<String>(...)
 *     find(node) { it == "Foo" } // Suspends and launches a coroutine to complete
 *     find(node) { it == "Bar" } // Suspends and launches a coroutine to complete
 * }
 * ```
 *
 * The code above runs sequentially, but a bunch of information is lost by delegating the execution to a coroutine,
 * in other words, it is very hard to track a bug because the *StackTrace* will have little to no relevant call-tree information.
 */
suspend inline fun <R> stackless(crossinline f: suspend () -> R): R =
    suspendCancellableCoroutine<R> {
        CoroutineScope(it.context).launch {
            try {
                it.resume(f())
            } catch (e: Exception) {
                it.cancel(e)
            }
        }
    }

/**
 * @see stackless
 */
suspend inline fun <R> stacklessIf(crossinline enabled: () -> Boolean, crossinline f: suspend () -> R): R =
    if (enabled()) {
        stackless(f)
    } else {
        f()
    }