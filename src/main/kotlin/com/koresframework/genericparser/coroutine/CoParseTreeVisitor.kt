/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine

import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTree
import org.antlr.v4.runtime.tree.RuleNode
import org.antlr.v4.runtime.tree.TerminalNode

interface CoParseTreeVisitor<T> {

    /**
     * Visit a parse tree, and return a user-defined result of the operation.
     *
     * @param tree The [ParseTree] to visit.
     * @return The result of visiting the parse tree.
     */
    suspend fun visit(tree: ParseTree): T?

    /**
     * Visit the children of a node, and return a user-defined result of the
     * operation.
     *
     * @param node The [RuleNode] whose children should be visited.
     * @return The result of visiting the children of the node.
     */
    suspend fun visitChildren(node: RuleNode): T?

    /**
     * Visit a terminal node, and return a user-defined result of the operation.
     *
     * @param node The [TerminalNode] to visit.
     * @return The result of visiting the node.
     */
    suspend fun visitTerminal(node: TerminalNode): T?

    /**
     * Visit an error node, and return a user-defined result of the operation.
     *
     * @param node The [ErrorNode] to visit.
     * @return The result of visiting the node.
     */
    suspend fun visitErrorNode(node: ErrorNode): T?

}