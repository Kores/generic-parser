/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine

import com.koresframework.genericparser.parser.GenericParser
import com.koresframework.genericparser.parser.GenericParser.*

interface CoGenericVisitor<T> : CoParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by [GenericParser.rootParameter].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitRootParameter(ctx: RootParameterContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.rootArgument].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitRootArgument(ctx: RootArgumentContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.literal].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLiteral(ctx: LiteralContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primitiveType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimitiveType(ctx: PrimitiveTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.numericType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitNumericType(ctx: NumericTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.integralType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitIntegralType(ctx: IntegralTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.floatingPointType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFloatingPointType(ctx: FloatingPointTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.referenceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitReferenceType(ctx: ReferenceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.referenceTypeWithIntersection].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitReferenceTypeWithIntersection(ctx: ReferenceTypeWithIntersectionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassOrInterfaceType(ctx: ClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassType(ctx: ClassTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classType_lf_classOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassType_lf_classOrInterfaceType(ctx: ClassType_lf_classOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classType_lfno_classOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassType_lfno_classOrInterfaceType(ctx: ClassType_lfno_classOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.interfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitInterfaceType(ctx: InterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.interfaceType_lf_classOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitInterfaceType_lf_classOrInterfaceType(ctx: InterfaceType_lf_classOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.interfaceType_lfno_classOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitInterfaceType_lfno_classOrInterfaceType(ctx: InterfaceType_lfno_classOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeVariable].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeVariable(ctx: TypeVariableContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayType(ctx: ArrayTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.dims].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitDims(ctx: DimsContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeParameter].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeParameter(ctx: TypeParameterContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeParameterModifier].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeParameterModifier(ctx: TypeParameterModifierContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeBound].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeBound(ctx: TypeBoundContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.additionalBound].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAdditionalBound(ctx: AdditionalBoundContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeArguments].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeArguments(ctx: TypeArgumentsContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeArgumentList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeArgumentList(ctx: TypeArgumentListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeArgument].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeArgument(ctx: TypeArgumentContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.wildcard].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitWildcard(ctx: WildcardContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.wildcardBounds].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitWildcardBounds(ctx: WildcardBoundsContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeName].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeName(ctx: TypeNameContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.packageOrTypeName].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPackageOrTypeName(ctx: PackageOrTypeNameContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.expressionName].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitExpressionName(ctx: ExpressionNameContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodName].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodName(ctx: MethodNameContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.ambiguousName].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAmbiguousName(ctx: AmbiguousNameContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeParameters].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeParameters(ctx: TypeParametersContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeParameterList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeParameterList(ctx: TypeParameterListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.variableDeclaratorId].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitVariableDeclaratorId(ctx: VariableDeclaratorIdContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.variableInitializer].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitVariableInitializer(ctx: VariableInitializerContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannType(ctx: UnannTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannPrimitiveType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannPrimitiveType(ctx: UnannPrimitiveTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannReferenceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannReferenceType(ctx: UnannReferenceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannClassOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannClassOrInterfaceType(ctx: UnannClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannClassType_lf_unannClassOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannClassType_lf_unannClassOrInterfaceType(ctx: UnannClassType_lf_unannClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannClassType_lfno_unannClassOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannClassType_lfno_unannClassOrInterfaceType(ctx: UnannClassType_lfno_unannClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannInterfaceType_lf_unannClassOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannInterfaceType_lf_unannClassOrInterfaceType(ctx: UnannInterfaceType_lf_unannClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannInterfaceType_lfno_unannClassOrInterfaceType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannInterfaceType_lfno_unannClassOrInterfaceType(ctx: UnannInterfaceType_lfno_unannClassOrInterfaceTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannTypeVariable].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannTypeVariable(ctx: UnannTypeVariableContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unannArrayType].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnannArrayType(ctx: UnannArrayTypeContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.formalParameterList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFormalParameterList(ctx: FormalParameterListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.formalParameters].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFormalParameters(ctx: FormalParametersContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.formalParameter].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFormalParameter(ctx: FormalParameterContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.variableModifier].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitVariableModifier(ctx: VariableModifierContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.lastFormalParameter].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLastFormalParameter(ctx: LastFormalParameterContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.receiverParameter].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitReceiverParameter(ctx: ReceiverParameterContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.annotation].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAnnotation(ctx: AnnotationContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.normalAnnotation].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitNormalAnnotation(ctx: NormalAnnotationContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.elementValuePairList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitElementValuePairList(ctx: ElementValuePairListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.elementValuePair].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitElementValuePair(ctx: ElementValuePairContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.elementValue].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitElementValue(ctx: ElementValueContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.elementValueArrayInitializer].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitElementValueArrayInitializer(ctx: ElementValueArrayInitializerContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.elementValueList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitElementValueList(ctx: ElementValueListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.markerAnnotation].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMarkerAnnotation(ctx: MarkerAnnotationContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.singleElementAnnotation].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitSingleElementAnnotation(ctx: SingleElementAnnotationContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayInitializer].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayInitializer(ctx: ArrayInitializerContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.variableInitializerList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitVariableInitializerList(ctx: VariableInitializerListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimary(ctx: PrimaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lf_arrayAccess].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lf_arrayAccess(ctx: PrimaryNoNewArray_lf_arrayAccessContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lfno_arrayAccess].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lfno_arrayAccess(ctx: PrimaryNoNewArray_lfno_arrayAccessContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lf_primary(ctx: PrimaryNoNewArray_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary(ctx: PrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary(ctx: PrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lfno_primary(ctx: PrimaryNoNewArray_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary(ctx: PrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.primaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary(ctx: PrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classInstanceCreationExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassInstanceCreationExpression(ctx: ClassInstanceCreationExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classInstanceCreationExpression_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassInstanceCreationExpression_lf_primary(ctx: ClassInstanceCreationExpression_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.classInstanceCreationExpression_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitClassInstanceCreationExpression_lfno_primary(ctx: ClassInstanceCreationExpression_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.typeArgumentsOrDiamond].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitTypeArgumentsOrDiamond(ctx: TypeArgumentsOrDiamondContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.fieldAccess].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFieldAccess(ctx: FieldAccessContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.fieldAccess_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFieldAccess_lf_primary(ctx: FieldAccess_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.fieldAccess_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitFieldAccess_lfno_primary(ctx: FieldAccess_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayAccess].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayAccess(ctx: ArrayAccessContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayAccess_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayAccess_lf_primary(ctx: ArrayAccess_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayAccess_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayAccess_lfno_primary(ctx: ArrayAccess_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodInvocation].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodInvocation(ctx: MethodInvocationContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodInvocation_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodInvocation_lf_primary(ctx: MethodInvocation_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodInvocation_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodInvocation_lfno_primary(ctx: MethodInvocation_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.argumentList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArgumentList(ctx: ArgumentListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodReference].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodReference(ctx: MethodReferenceContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodReference_lf_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodReference_lf_primary(ctx: MethodReference_lf_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.methodReference_lfno_primary].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMethodReference_lfno_primary(ctx: MethodReference_lfno_primaryContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.arrayCreationExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitArrayCreationExpression(ctx: ArrayCreationExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.dimExprs].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitDimExprs(ctx: DimExprsContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.dimExpr].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitDimExpr(ctx: DimExprContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.expression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitExpression(ctx: ExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.lambdaExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLambdaExpression(ctx: LambdaExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.lambdaParameters].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLambdaParameters(ctx: LambdaParametersContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.inferredFormalParameterList].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitInferredFormalParameterList(ctx: InferredFormalParameterListContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.lambdaBody].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLambdaBody(ctx: LambdaBodyContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.assignmentExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAssignmentExpression(ctx: AssignmentExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.assignment].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAssignment(ctx: AssignmentContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.leftHandSide].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitLeftHandSide(ctx: LeftHandSideContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.assignmentOperator].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAssignmentOperator(ctx: AssignmentOperatorContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.conditionalExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitConditionalExpression(ctx: ConditionalExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.conditionalOrExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitConditionalOrExpression(ctx: ConditionalOrExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.conditionalAndExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitConditionalAndExpression(ctx: ConditionalAndExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.inclusiveOrExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitInclusiveOrExpression(ctx: InclusiveOrExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.exclusiveOrExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitExclusiveOrExpression(ctx: ExclusiveOrExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.andExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAndExpression(ctx: AndExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.equalityExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitEqualityExpression(ctx: EqualityExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.relationalExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitRelationalExpression(ctx: RelationalExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.shiftExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitShiftExpression(ctx: ShiftExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.additiveExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitAdditiveExpression(ctx: AdditiveExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.multiplicativeExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitMultiplicativeExpression(ctx: MultiplicativeExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unaryExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnaryExpression(ctx: UnaryExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.preIncrementExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPreIncrementExpression(ctx: PreIncrementExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.preDecrementExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPreDecrementExpression(ctx: PreDecrementExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.unaryExpressionNotPlusMinus].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitUnaryExpressionNotPlusMinus(ctx: UnaryExpressionNotPlusMinusContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.postfixExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPostfixExpression(ctx: PostfixExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.postIncrementExpression_lf_postfixExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPostIncrementExpression_lf_postfixExpression(ctx: PostIncrementExpression_lf_postfixExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.postDecrementExpression_lf_postfixExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitPostDecrementExpression_lf_postfixExpression(ctx: PostDecrementExpression_lf_postfixExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.castExpression].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitCastExpression(ctx: CastExpressionContext): T?

    /**
     * Visit a parse tree produced by [GenericParser.identifier].
     * @param ctx the parse tree
     * @return the visitor result
     */
    suspend fun visitIdentifier(ctx: IdentifierContext): T?
}