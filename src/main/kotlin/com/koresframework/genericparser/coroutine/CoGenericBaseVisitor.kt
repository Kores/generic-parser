/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine

import com.koresframework.genericparser.parser.GenericParser.*

open class CoGenericBaseVisitor<T> : CoSealingVisitor<T>(), CoGenericVisitor<T> {

    /*
    *
    { @inheritDoc }
    *
    * <p>The
    var returns: implementation? = null   var result:the? = null   var calling:of? = null
    *
    { @link # visitChildren }
    fun on() {
    }.</p>
    */
    override suspend fun visitRootParameter(ctx: RootParameterContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitRootArgument(ctx: RootArgumentContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLiteral(ctx: LiteralContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimitiveType(ctx: PrimitiveTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitNumericType(ctx: NumericTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitIntegralType(ctx: IntegralTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFloatingPointType(ctx: FloatingPointTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitReferenceType(ctx: ReferenceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitReferenceTypeWithIntersection(ctx: ReferenceTypeWithIntersectionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassOrInterfaceType(ctx: ClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassType(ctx: ClassTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassType_lf_classOrInterfaceType(ctx: ClassType_lf_classOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassType_lfno_classOrInterfaceType(ctx: ClassType_lfno_classOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitInterfaceType(ctx: InterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitInterfaceType_lf_classOrInterfaceType(ctx: InterfaceType_lf_classOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitInterfaceType_lfno_classOrInterfaceType(ctx: InterfaceType_lfno_classOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeVariable(ctx: TypeVariableContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayType(ctx: ArrayTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitDims(ctx: DimsContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeParameter(ctx: TypeParameterContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeParameterModifier(ctx: TypeParameterModifierContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeBound(ctx: TypeBoundContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAdditionalBound(ctx: AdditionalBoundContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeArguments(ctx: TypeArgumentsContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeArgumentList(ctx: TypeArgumentListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeArgument(ctx: TypeArgumentContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitWildcard(ctx: WildcardContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitWildcardBounds(ctx: WildcardBoundsContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeName(ctx: TypeNameContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPackageOrTypeName(ctx: PackageOrTypeNameContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitExpressionName(ctx: ExpressionNameContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodName(ctx: MethodNameContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAmbiguousName(ctx: AmbiguousNameContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeParameters(ctx: TypeParametersContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeParameterList(ctx: TypeParameterListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitVariableDeclaratorId(ctx: VariableDeclaratorIdContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitVariableInitializer(ctx: VariableInitializerContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannType(ctx: UnannTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannPrimitiveType(ctx: UnannPrimitiveTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannReferenceType(ctx: UnannReferenceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannClassOrInterfaceType(ctx: UnannClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannClassType_lf_unannClassOrInterfaceType(ctx: UnannClassType_lf_unannClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannClassType_lfno_unannClassOrInterfaceType(ctx: UnannClassType_lfno_unannClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannInterfaceType_lf_unannClassOrInterfaceType(ctx: UnannInterfaceType_lf_unannClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannInterfaceType_lfno_unannClassOrInterfaceType(ctx: UnannInterfaceType_lfno_unannClassOrInterfaceTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannTypeVariable(ctx: UnannTypeVariableContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnannArrayType(ctx: UnannArrayTypeContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFormalParameterList(ctx: FormalParameterListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFormalParameters(ctx: FormalParametersContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFormalParameter(ctx: FormalParameterContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitVariableModifier(ctx: VariableModifierContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLastFormalParameter(ctx: LastFormalParameterContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitReceiverParameter(ctx: ReceiverParameterContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAnnotation(ctx: AnnotationContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitNormalAnnotation(ctx: NormalAnnotationContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitElementValuePairList(ctx: ElementValuePairListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitElementValuePair(ctx: ElementValuePairContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitElementValue(ctx: ElementValueContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitElementValueArrayInitializer(ctx: ElementValueArrayInitializerContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitElementValueList(ctx: ElementValueListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMarkerAnnotation(ctx: MarkerAnnotationContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitSingleElementAnnotation(ctx: SingleElementAnnotationContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayInitializer(ctx: ArrayInitializerContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitVariableInitializerList(ctx: VariableInitializerListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimary(ctx: PrimaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lf_arrayAccess(ctx: PrimaryNoNewArray_lf_arrayAccessContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lfno_arrayAccess(ctx: PrimaryNoNewArray_lfno_arrayAccessContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lf_primary(ctx: PrimaryNoNewArray_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary(ctx: PrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary(ctx: PrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lfno_primary(ctx: PrimaryNoNewArray_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary(ctx: PrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary(ctx: PrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassInstanceCreationExpression(ctx: ClassInstanceCreationExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassInstanceCreationExpression_lf_primary(ctx: ClassInstanceCreationExpression_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitClassInstanceCreationExpression_lfno_primary(ctx: ClassInstanceCreationExpression_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitTypeArgumentsOrDiamond(ctx: TypeArgumentsOrDiamondContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFieldAccess(ctx: FieldAccessContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFieldAccess_lf_primary(ctx: FieldAccess_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitFieldAccess_lfno_primary(ctx: FieldAccess_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayAccess(ctx: ArrayAccessContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayAccess_lf_primary(ctx: ArrayAccess_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayAccess_lfno_primary(ctx: ArrayAccess_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodInvocation(ctx: MethodInvocationContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodInvocation_lf_primary(ctx: MethodInvocation_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodInvocation_lfno_primary(ctx: MethodInvocation_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArgumentList(ctx: ArgumentListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodReference(ctx: MethodReferenceContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodReference_lf_primary(ctx: MethodReference_lf_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMethodReference_lfno_primary(ctx: MethodReference_lfno_primaryContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitArrayCreationExpression(ctx: ArrayCreationExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitDimExprs(ctx: DimExprsContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitDimExpr(ctx: DimExprContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitExpression(ctx: ExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLambdaExpression(ctx: LambdaExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLambdaParameters(ctx: LambdaParametersContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitInferredFormalParameterList(ctx: InferredFormalParameterListContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLambdaBody(ctx: LambdaBodyContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAssignmentExpression(ctx: AssignmentExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAssignment(ctx: AssignmentContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitLeftHandSide(ctx: LeftHandSideContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAssignmentOperator(ctx: AssignmentOperatorContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitConditionalExpression(ctx: ConditionalExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitConditionalOrExpression(ctx: ConditionalOrExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitConditionalAndExpression(ctx: ConditionalAndExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitInclusiveOrExpression(ctx: InclusiveOrExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitExclusiveOrExpression(ctx: ExclusiveOrExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAndExpression(ctx: AndExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitEqualityExpression(ctx: EqualityExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitRelationalExpression(ctx: RelationalExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitShiftExpression(ctx: ShiftExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitAdditiveExpression(ctx: AdditiveExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitMultiplicativeExpression(ctx: MultiplicativeExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnaryExpression(ctx: UnaryExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPreIncrementExpression(ctx: PreIncrementExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPreDecrementExpression(ctx: PreDecrementExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitUnaryExpressionNotPlusMinus(ctx: UnaryExpressionNotPlusMinusContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPostfixExpression(ctx: PostfixExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPostIncrementExpression_lf_postfixExpression(ctx: PostIncrementExpression_lf_postfixExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitPostDecrementExpression_lf_postfixExpression(ctx: PostDecrementExpression_lf_postfixExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitCastExpression(ctx: CastExpressionContext): T? {
        return visitChildren(ctx)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation returns the result of calling
     * [.visitChildren] on `ctx`.
     */
    override suspend fun visitIdentifier(ctx: IdentifierContext): T? {
        return visitChildren(ctx)
    }

}