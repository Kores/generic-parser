/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine

import com.koresframework.genericparser.parser.GenericVisitor
import org.antlr.v4.runtime.tree.*
import java.lang.reflect.Method
import java.lang.reflect.Proxy
import kotlin.coroutines.Continuation

/**
 * Creates an adapter that passes the [continuation][cont] to [visitor] when a method of the interfaces
 * implemented by the generated proxy is called.
 *
 * This is a workaround for maintaining CPS (Continuation Passing Style) when a [ParseTreeVisitor] or [GenericVisitor] must
 * be passed to a `visit` function.
 */
fun <T, S> createAdapter(cont: Continuation<in T>, visitor: CoParseTreeVisitor<T>): S where S : ParseTreeVisitor<T>, S : GenericVisitor<T> {
    val methodCache = mutableMapOf<Pair<String, Array<Class<*>>>, Method>()
    val instance = Proxy.newProxyInstance(
        visitor::class.java.classLoader,
        arrayOf(ParseTreeVisitor::class.java, GenericVisitor::class.java)
    ) { instance, method, args ->
        val key = method.name to (method.parameterTypes + Continuation::class.java)
        val newMethod = methodCache.computeIfAbsent(key) { (name, parameters) ->
            visitor::class.java.getMethod(name, *parameters)
        }

        newMethod.invoke(visitor, *(args + cont))
    }

    return instance as S
}