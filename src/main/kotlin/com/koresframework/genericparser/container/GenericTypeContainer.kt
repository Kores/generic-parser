/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.container

import com.koresframework.genericparser.AddBoundFunction
import com.koresframework.genericparser.BoundKind
import com.koresframework.genericparser.ContainerFactory
import com.koresframework.genericparser.WildcardTypeOrName
import java.util.*

typealias GenericParserType = GenericType
typealias GenericParserTypeVariable = GenericType.TypeVariable
typealias GenericParserClassType = GenericType.ClassType
typealias GenericParserWildcard = GenericType.Wildcard

typealias GenericParserBound = Bound
typealias GenericParserBoundOf = Bound.Of
typealias GenericParserBoundExtends = Bound.Extends
typealias GenericParserBoundSuper = Bound.Super

sealed class GenericType(val name: String, val bounds: List<Bound> = listOf()) {
    class Wildcard(bounds: List<Bound> = listOf()): GenericType("?", bounds) {
        override fun addBound(bound: Bound): GenericType = Wildcard(this.bounds + bound)
        override fun toString(): String =
            "$name${boundsToString(true)}"
    }
    class ClassType(type: String, bounds: List<Bound> = listOf()): GenericType(type, bounds) {
        override fun addBound(bound: Bound): GenericType = ClassType(name, this.bounds + bound)
    }
    class TypeVariable(type: String, bounds: List<Bound> = listOf()): GenericType(type, bounds) {
        override fun addBound(bound: Bound): GenericType = TypeVariable(name, this.bounds + bound)
    }

    abstract fun addBound(bound: Bound): GenericType

    override fun hashCode(): Int = Objects.hash(this::class.simpleName, this.name, this.bounds)
    override fun equals(other: Any?): Boolean =
        other is GenericType
                && other::class.simpleName == this::class.simpleName
                && other.name == this.name
                && other.bounds == this.bounds

    override fun toString(): String =
        "$name${boundsToString()}"

    fun boundsToString(wild: Boolean = false) =
        if (bounds.isEmpty()) ""
        else if (bounds.all { it is Bound.Extends }) " extends " + bounds.joinToString(separator = " & ") {
            it.genericType.toString()
        }
        else if (bounds.all { it is Bound.Super }) " super " + bounds.joinToString(separator = " & ") {
            it.genericType.toString()
        }
        else if (bounds.all { it is Bound.Of }) bounds.joinToString(prefix = "<", separator = ", ", postfix = ">")
        else if (wild) " " + bounds.joinToString(separator = " & ")
        else bounds.joinToString(prefix = "<", separator = ", ", postfix = ">")
}

sealed class Bound(val genericType: GenericType, val sign: String) {
    class Of(genericType: GenericType): Bound(genericType, "") {
        override fun toString(): String = "$genericType"
    }
    class Extends(genericType: GenericType): Bound(genericType, "+") {
        override fun toString(): String = "extends $genericType"
    }
    class Super(genericType: GenericType): Bound(genericType, "-") {
        override fun toString(): String = "super $genericType"
    }

    override fun hashCode(): Int = Objects.hash(this.sign, this.genericType)
    override fun equals(other: Any?): Boolean =
        other is Bound && other.sign == this.sign && other.genericType == this.genericType
}

val GenericTypeFactory = ContainerFactory { wildcardTypeOrName ->
    when(wildcardTypeOrName) {
        is WildcardTypeOrName.Wildcard -> GenericType.Wildcard()
        is WildcardTypeOrName.Name -> GenericType.TypeVariable(wildcardTypeOrName.name)
        is WildcardTypeOrName.Type -> GenericType.ClassType(wildcardTypeOrName.type)
    }
}

val GenericTypeAddBoundFunction = AddBoundFunction<GenericType> { root, kind, typeToBound ->
    when (kind) {
        BoundKind.Of -> root.addBound(Bound.Of(typeToBound))
        BoundKind.Extends -> root.addBound(Bound.Extends(typeToBound))
        BoundKind.Super -> root.addBound(Bound.Super(typeToBound))
    }
}