/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser.coroutine;

import kotlin.coroutines.Continuation;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CoStealer<C> extends CoAbstractParseTreeVisitor<C> {
    public CoStealer() {
        super();
    }

    @Nullable
    @Override
    public Object visit(@NotNull ParseTree tree, @NotNull Continuation<? super C> $completion) {
        return $completion;
    }

    @Nullable
    @Override
    public Object visitChildren(@NotNull RuleNode node, @NotNull Continuation<? super C> $completion) {
        return $completion;
    }

    @Nullable
    @Override
    public Object visitTerminal(@NotNull TerminalNode node, @NotNull Continuation<? super C> $completion) {
        return super.visitTerminal(node, $completion);
    }

    @Nullable
    @Override
    public Object visitErrorNode(@NotNull ErrorNode node, @NotNull Continuation<? super C> $completion) {
        return super.visitErrorNode(node, $completion);
    }

    @Nullable
    @Override
    protected Object defaultResult(@NotNull Continuation<? super C> $completion) {
        return super.defaultResult($completion);
    }

    @Nullable
    @Override
    protected Object aggregateResult(@Nullable C aggregate, @Nullable C nextResult, @NotNull Continuation<? super C> $completion) {
        return super.aggregateResult(aggregate, nextResult, $completion);
    }

    @Nullable
    @Override
    protected Object shouldVisitNextChild(@NotNull RuleNode node, @Nullable C currentResult, @NotNull Continuation<? super Boolean> $completion) {
        return super.shouldVisitNextChild(node, currentResult, $completion);
    }
}
