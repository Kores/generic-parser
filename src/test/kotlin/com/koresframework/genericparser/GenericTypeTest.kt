/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser

import com.koresframework.genericparser.parser.VisitorGenericRepresentationParser
import com.koresframework.genericparser.container.*
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class GenericTypeTest : FunSpec({
    test("<? extends List<String> & List<Integer>>") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(
                GenericTypeFactory,
                GenericTypeAddBoundFunction,
                stacklessMode = stacklessMode
            )
            val r =
                c.parse("<? extends List<String> & List<Integer>>", ParseOptions(parseMode = ParseMode.TypeArgument))
            val listOfString = GenericParserClassType("List").addBound(GenericParserBoundOf(GenericParserClassType("String")))
            val listOfInteger = GenericParserClassType("List").addBound(GenericParserBoundOf(GenericParserClassType("Integer")))
            val anyExtendsListOfStringAndListOfInteger = GenericParserWildcard()
                .addBound(GenericParserBoundExtends(listOfString)).addBound(GenericParserBoundExtends(listOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(anyExtendsListOfStringAndListOfInteger)
                r.single().toString().shouldBe("? extends List<String> & List<Integer>")
            }
        }
    }

})
