/**
 *      generic-parser - Parse Java Generic Representation and transform into objects.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) JonathanxD <https://github.com/JonathanxD/>
 *      Copyright (c) KoresFramework <https://github.com/koresframework/>
 *      Copyright (c) contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.genericparser

import com.koresframework.genericparser.parser.VisitorGenericRepresentationParser
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import java.util.*

class WithTest : FunSpec({
    test("<? extends List<String> & List<Integer>>") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r =
                c.parse("<? extends List<String> & List<Integer>>", ParseOptions(parseMode = ParseMode.TypeArgument))
            val listOfString = TypeContainer.OfType("List").withBounds(Bound.Of(TypeContainer.OfType("String")))
            val listOfInteger = TypeContainer.OfType("List").withBounds(Bound.Of(TypeContainer.OfType("Integer")))
            val anyExtendsListOfStringAndListOfInteger = TypeContainer.Wildcard()
                .withBounds(Bound.Extends(listOfString), Bound.Extends(listOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(anyExtendsListOfStringAndListOfInteger)
                r.single().toString().shouldBe("? extends List<String> & extends List<Integer>")
            }
        }
    }
    test("<T extends List<String> & List<Integer>> with T as knownTypeVariable") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse("<T extends List<String> & List<Integer>>", ParseOptions(knownTypeVariables = setOf("T")))
            val listOfString = TypeContainer.OfType("List").withBounds(Bound.Of(TypeContainer.OfType("String")))
            val listOfInteger = TypeContainer.OfType("List").withBounds(Bound.Of(TypeContainer.OfType("Integer")))
            val tExtendsListOfStringAndListOfInteger = TypeContainer.OfName("T")
                .withBounds(Bound.Extends(listOfString), Bound.Extends(listOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(tExtendsListOfStringAndListOfInteger)
                r.single().toString().shouldBe("T extends List<String> & extends List<Integer>")
            }
        }
    }
    test("<LList extends List<String> & List<Integer>>") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse(
                "<LList extends List<String> & List<Integer>>",
                ParseOptions(parseMode = ParseMode.TypeArgument, knownTypeVariables = setOf("LList"))
            )

            val IntegerT = TypeContainer.OfType("Integer")
            val StringT = TypeContainer.OfType("String")
            val List = TypeContainer.OfType("List")
            val ListOfString = List.newWithBounds(Bound.Of(StringT))
            val ListOfInteger = List.newWithBounds(Bound.Of(IntegerT))
            val ListExtendsList = TypeContainer.OfName("LList").withBounds(Bound.Extends(ListOfString), Bound.Extends(ListOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly three elements") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(ListExtendsList)
                r.map { it.toString() }.shouldBe(
                    listOf("LList extends List<String> & extends List<Integer>")
                )
            }
        }
    }

    test("<List extends List<String> & List<Integer>>") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse(
                "<List extends List<String> & List<Integer>>",
                ParseOptions(parseMode = ParseMode.TypeArgument, knownTypeVariables = setOf("List"))
            )

            val IntegerT = TypeContainer.OfType("Integer")
            val StringT = TypeContainer.OfType("String")
            val List = TypeContainer.OfName("List")
            val ListOfString = List.newWithBounds(Bound.Of(StringT))
            val ListOfInteger = List.newWithBounds(Bound.Of(IntegerT))
            val ListExtendsList = TypeContainer.OfName("List").withBounds(Bound.Extends(ListOfString), Bound.Extends(ListOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly three elements") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(ListExtendsList)
                r.map { it.toString() }.shouldBe(
                    listOf("List extends List<String> & extends List<Integer>")
                )
            }
        }
    }

    xtest("coroutine(<List extends List<String> & List<Integer>>)") {
        val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = false)
        shouldThrow<UnknownTypeKindException> {
            val c = c.parse("<List extends List<String> & List<Integer>>",
                ParseOptions(parseMode = ParseMode.TypeArgument, knownTypeVariables = setOf("T")))
            println(c)
        }
    }

    test("coroutine(T extends List<String> & List<Integer>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse(
                "T extends List<String> & List<Integer>",
                ParseOptions(parseMode = ParseMode.TypeArgument, knownTypeVariables = setOf("T"))
            )

            val IntegerT = TypeContainer.OfType("Integer")
            val StringT = TypeContainer.OfType("String")
            val List = TypeContainer.OfType("List")
            val ListOfString = List.newWithBounds(Bound.Of(StringT))
            val ListOfInteger = List.newWithBounds(Bound.Of(IntegerT))
            val TExtendsList = TypeContainer.OfName("T").withBounds(Bound.Extends(ListOfString), Bound.Extends(ListOfInteger))

            assertSoftly {
                withClue("Parsed types should have exactly three elements") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(TExtendsList)
                r.map { it.toString() }.shouldBe(
                    listOf("T extends List<String> & extends List<Integer>")
                )
            }
        }
    }

    test("coroutine(<B extends K & V, K, V>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse("<B extends K & V, K, V>", ParseOptions(parseMode = ParseMode.GenericSignature))

            val B = TypeContainer.OfName("B")
            val K = TypeContainer.OfName("K")
            val V = TypeContainer.OfName("V")
            val BExtendsKV = B.withBounds(Bound.Extends(K), Bound.Extends(V))

            assertSoftly {
                withClue("Parsed types should have exactly three elements") {
                    r.size.shouldBe(3)
                }
                r.shouldBe(listOf(
                    BExtendsKV,
                    K,
                    V
                ))
                r.map { it.toString() }.shouldBe(
                    listOf("B extends K & extends V", "K", "V")
                )
            }
        }
    }

    test("coroutine(<E extends List<? extends Instruction & KoresElement>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse(
                "<E extends List<? extends Instruction & KoresElement>>",
                ParseOptions(parseMode = ParseMode.GenericSignature)
            )

            val Instruction = TypeContainer.OfType("Instruction")
            val KoresElement = TypeContainer.OfType("KoresElement")
            val List = TypeContainer.OfType("List")
            val Wild = TypeContainer.Wildcard().withBounds(Bound.Extends(Instruction), Bound.Extends(KoresElement))
            val ListOfWild = List.withBounds(Bound.Of(Wild))
            val EExtendsList = TypeContainer.OfName("E").withBounds(Bound.Extends(ListOfWild))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(EExtendsList)
                r.single().toString().shouldBe("E extends List<? extends Instruction & extends KoresElement>")
            }
        }
    }

    test("coroutine(<E extends List<? extends Instruction & KoresElement<E>>>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse(
                "<E extends List<? extends Instruction & KoresElement<E>>>",
                ParseOptions(parseMode = ParseMode.GenericSignature)
            )

            val Instruction = TypeContainer.OfType("Instruction")
            val KoresElement = TypeContainer.OfType("KoresElement")
            val KoresElementOfE = KoresElement.withBounds(Bound.Of(TypeContainer.OfName("E")))
            val List = TypeContainer.OfType("List")
            val Wild = TypeContainer.Wildcard().withBounds(Bound.Extends(Instruction), Bound.Extends(KoresElementOfE))
            val ListOfWild = List.withBounds(Bound.Of(Wild))
            val EExtendsList = TypeContainer.OfName("E").withBounds(Bound.Extends(ListOfWild))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(EExtendsList)
                r.single().toString().shouldBe("E extends List<? extends Instruction & extends KoresElement<E>>")
            }
        }
    }

    test("coroutine(List<? extends Instruction>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse("List<? extends Instruction>", ParseOptions(parseMode = ParseMode.TypeArgument))

            val wild = TypeContainer.Wildcard().withBounds(Bound.Extends(TypeContainer.OfType("Instruction")))
            val listOfWildExtendsInsn = TypeContainer.OfType("List").withBounds(Bound.Of(wild))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(listOfWildExtendsInsn)
                r.single().toString().shouldBe("List<? extends Instruction>")
            }
        }
    }

    test("coroutine(java.util.List<? extends kores.Instruction>)") {
        for (stacklessMode in arrayOf(false, true)) {
            val c = VisitorGenericRepresentationParser(F1(), F2(), stacklessMode = stacklessMode)
            val r = c.parse("java.util.List<? extends kores.Instruction>", ParseOptions(parseMode = ParseMode.TypeArgument))

            val wild = TypeContainer.Wildcard().withBounds(Bound.Extends(TypeContainer.OfType("kores.Instruction")))
            val listOfWildExtendsInsn = TypeContainer.OfType("java.util.List").withBounds(Bound.Of(wild))

            assertSoftly {
                withClue("Parsed types should have exactly one element") {
                    r.size.shouldBe(1)
                }
                r.single().shouldBe(listOfWildExtendsInsn)
                r.single().toString().shouldBe("java.util.List<? extends kores.Instruction>")
            }
        }
    }
})

sealed class Bound {
    abstract val type: TypeContainer

    data class Extends(override val type: TypeContainer): Bound() {
        override fun toString(): String = "extends $type"
    }

    data class Super(override val type: TypeContainer): Bound(){
        override fun toString(): String = "super $type"
    }

    data class Of(override val type: TypeContainer): Bound(){
        override fun toString(): String = "$type"
    }
}


sealed class TypeContainer {
    val bounds = mutableListOf<Bound>()

    fun addBound(bound: Bound) {
        this.bounds += bound
    }

    fun withBounds(bounds: List<Bound>): TypeContainer {
        this.bounds.addAll(bounds)
        return this
    }

    fun withBounds(vararg bound: Bound): TypeContainer {
        this.bounds.addAll(bound)
        return this
    }

    fun newWithBounds(vararg bound: Bound): TypeContainer {
        val clone = this.copy()
        clone.bounds.addAll(bound)
        return clone
    }

    abstract fun copy(): TypeContainer

    class Wildcard : TypeContainer() {
        override fun toString(): String =
            "?${boundsToString(true)}"

        override fun equals(other: Any?): Boolean =
            other is Wildcard && other.bounds == this.bounds

        override fun hashCode(): Int =
            Objects.hash(ContainerKind.Wildcard, this.bounds)

        override fun copy(): TypeContainer = Wildcard().withBounds(this.bounds)
    }

    class OfName(val name: String) : TypeContainer() {
        override fun toString(): String =
            "$name${boundsToString(true)}"

        override fun equals(other: Any?): Boolean =
            other is OfName&& this.name == other.name && other.bounds == this.bounds

        override fun hashCode(): Int =
            Objects.hash(ContainerKind.Name, this.name, this.bounds)

        override fun copy(): TypeContainer = OfName(name).withBounds(this.bounds)
    }

    class OfType(val type: String) : TypeContainer() {
        override fun toString(): String =
            "$type${boundsToString()}"

        override fun equals(other: Any?): Boolean =
            other is OfType && this.type == other.type && other.bounds == this.bounds

        override fun hashCode(): Int =
            Objects.hash(ContainerKind.Type, this.type, this.bounds)

        override fun copy(): TypeContainer = OfType(type).withBounds(this.bounds)
    }

    fun boundsToString(wild: Boolean = false) =
        if (bounds.isEmpty()) ""
        else if (!wild) bounds.joinToString(prefix = "<", separator = " & ", postfix = ">")
        else if (bounds.all { it is Bound.Of }) bounds.joinToString(prefix = "<", separator = " & ", postfix = ">")
        else " " + bounds.joinToString(separator = " & ")

    override fun toString(): String =
        "${this::class.simpleName}[${bounds.joinToString(" & ")}]"
}

enum class ContainerKind {
    Wildcard,
    Type,
    Name
}

class F1 : ContainerFactory<TypeContainer> {
    override fun createGenericContainer(wildcardTypeOrName: WildcardTypeOrName): TypeContainer =
        when(wildcardTypeOrName) {
            is WildcardTypeOrName.Type -> TypeContainer.OfType(wildcardTypeOrName.type)
            is WildcardTypeOrName.Name -> TypeContainer.OfName(wildcardTypeOrName.name)
            is WildcardTypeOrName.Wildcard -> TypeContainer.Wildcard()
        }
}

class F2 : AddBoundFunction<TypeContainer> {
    override fun addBound(
        genericContainer: TypeContainer,
        boundKind: BoundKind,
        type: TypeContainer
    ): TypeContainer {
        when (boundKind) {
            BoundKind.Of -> genericContainer.addBound(Bound.Of(type))
            BoundKind.Extends -> genericContainer.addBound(Bound.Extends(type))
            BoundKind.Super -> genericContainer.addBound(Bound.Super(type))
        }

        return genericContainer
    }
}
